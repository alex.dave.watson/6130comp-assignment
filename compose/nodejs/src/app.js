var systemLeader = false;

//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express');

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//used for the file reader
const fs = require('fs');

//Get the hostname of the node
const os = require('os');

//Required for messege queueing for RabbitMQ
const amqp = require('amqplib/callback_api');

//Docker API
const request = require('request');
const { Console } = require('console');
//const axios = require('axios').default; //newer that request, 
//Create / stop required for specifying the container structure, for creating new container / stopping.
var create = {};
var stop = {};

//Set times for scaling based on the spec.
var scaleUpTime = new Date();
var scaleDownTime = new Date();
scaleUpTime.setHours(16,00,00); //4pm
scaleDownTime.setHours(18,00,00); //6pm
var currentlyScaledUp = false;

//This is the URL endopint of your vm running docker
const url = 'http://192.168.56.10:2375';

//instance of express and port to use for inbound connections.
const app = express();
const port = 3000;

//bind node to the port
app.listen(port, () => {
  console.log('Express listening at port'  + port);
})

//Get the details of the host
var myhostname = os.hostname();

var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
//print the hostname
console.log(myhostname);

//Parse the file into a structure
nodeTxtFile = fs.readFileSync('node.txt');
nodes = JSON.parse(nodeTxtFile);

//Delay leadership till system is booted.
var booted = false;
//setTimeout(()=> {booted = true}, 10000); //wait 10 seconds before doing leadership stuff

//Container name.
var newNodeNameCounter = 4; //3 containers are always active. so new one is the 4th.
var newContainerName = "AppNode";

//connection string listing the mongo servers.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/NotFlixDB?replicaSet=rs0';

var currentTime = new Date().getTime() / 1000;

//Node object information list
var NodeInformation = {"hostname": myhostname, "nodeIdentifier": nodeID, "timeSinceMessege": currentTime, "Alive": true};
var NodeAliveList = [];
NodeAliveList.push(NodeInformation); //Push the current clusters info to the list.

//Read Alive Messages, add node to alive list if not already there.
amqp.connect('amqp://user:bitnami@RabbitMQ_HAProxy', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var exchange = 'alive';
    channel.assertExchange(exchange, 'fanout', {
      durable: false
    });
    channel.assertQueue('', {
      exclusive: true
    }, function(error2, q) {
      if (error2) {
        throw error2;
      }
      booted = true; //If we are here, then the rabbitMQ has booted up, so we are ready to do leadership stuff.
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');
      channel.consume(q.queue, function(msg) {
        if(msg.content) {
          console.log('Reading Alive Messeges: ' + myhostname);
          var incommingNode = JSON.parse(msg.content);
          currentTime = new Date().getTime() / 1000;
          var nodeInList = false;
          if (incommingNode.hostName == myhostname) { //Its my node message.
            NodeInformation.timeSinceMessege = currentTime;
          } 
          else {        
            Object.entries(NodeAliveList).forEach(([hostName, prop]) => {
              if (hostName != myhostname) { 
                if (prop.nodeIdentifier == incommingNode.nodeIdentifier){ //If the node is found, update it in the list.       
                  prop.Alive = true; 
                  prop.timeSinceMessege = currentTime;
                  nodeInList = true;
                }
              }
            });
            if (nodeInList == false) { //If it is not found, add it to the node list.
              NodeAliveList.push({"hostname": incommingNode.hostName, "nodeIdentifier": incommingNode.nodeIdentifier, "timeSinceMessege": currentTime, "Alive": true});
            }        
          }
        }
      }, {
        noAck: true
      });
    });
  });
});

//Every 7 seconds, check list of alive nodes, and check if i am the leader.
setInterval(function(){
  if (booted)
  {
    var maxNodeID = 0;
    currentTime = new Date().getTime() / 1000;
    var leader = true;
    Object.entries(NodeAliveList).forEach(([nodeIdentifier, prop]) => {
      var tempNodeID = Number(prop.nodeIdentifier);
      if (prop.hostname != myhostname) {
        var elapsedTime = currentTime - prop.timeSinceMessege; //Do not need to divide by 1000 as the time is saved like this.
        var elapsedTimeInSeconds = Math.round(elapsedTime);
        if (tempNodeID > maxNodeID) { //if number is greater, and the node is "Alive", then i am not the leader.
          maxNodeID = tempNodeID;
        }
      } 
    });
    if (nodeID >= maxNodeID) {//You are the leader.
      leader = true;
      systemLeader = true;
    }
    else { //You are not the leader.
      leader = false;
      systemLeader = false;
    }
    if (leader == true) {
      console.log("I am the system leader:" + nodeID);
      console.log("Number Of Nodes Found: " + NodeAliveList.length); //ensures that the developer can see the leaders list has items in.
      systemLeader = true; //Enables Leadership functionality.
    }
  }
}, 7000);

//Every 3 seconds, if i am the leader, verify the nodes are alive, and communicate with the docker api
setInterval(function(){
  if(systemLeader) {
    var foundDeadNode = false;
    Object.entries(NodeAliveList).forEach(([nodeIdentifier, prop]) => {
      var elapsedTime = currentTime - prop.timeSinceMessege; //Do not need to divide by 1000 as the time is saved like this.
      var elapsedTimeInSeconds = Math.round(elapsedTime);
      if (elapsedTimeInSeconds > 15) { //check that the node has had a message in the last 15 seconds.     
        prop.Alive = false;
        foundDeadNode = true;
        console.log("Node no longer alive:" + nodeIdentifier);
      }
      else {
        prop.Alive = true;
      }
    });
    if (foundDeadNode){
      //THis is not currently working.
      Object.entries(NodeAliveList).forEach(([nodeIdentifier, prop]) => {
        if (prop.Alive == false) {
          console.log("Failed node hostname: " + prop.hostname);
          var start = {
            uri: url + "/v1.41/containers/" + prop.hostname + "/start",
	      	  method: 'POST',
	          json: {}
	        };         
          //send the start request for the dead node.
          request(start, function (error, response, startBody) {
            var errorOccured = false;
            if (error) {
              console.log("Request Error. Failed to start.");
              errorOccured = true;
            }
	          if (!error) {
		          console.log("Container start completed. " + prop.hostname.toString());
              //Post a wait on the container, so the container is defo started.
              var wait = {
			          uri: url + "/v1.41/containers/" + prop.hostname + "/wait",
                method: 'POST',
                json: {}
		          };       
			        request(wait, function (error, response, waitBody ) {
			          if (!error) {
				          console.log("Waited for container restart, container will have started.");		            
                  //send a simple get request for stdout from the container
                  request.get({
                    //url: url + "/v1.41/containers/" +prop.hostname + "/logs?stdout=1",
                    url: url + "/v1.41/containers/" + prop.hostname + "/logs?stdout=1",
                  }, (err, res, data) => {
                    if (err) {
                      errorOccured = true;
                      console.log("Container Name: " + prop.hostname);
                      console.log('Error:', err);
                    } else if (res.statusCode == 404) { //Failed to find container, make a new one.
                      console.log('Status:', res.statusCode);
                      console.log("Failed to restart container, it was not found.");
                      console.log("Creating new container.");  
                      //This code is test code, and has yet to be fully tested
                       create = {
                        uri: url + "/v1.40/containers/create",
                        method: 'POST',
                        //deploy an alpine container that runs my app.js code. Give it a new name.
                        data: {"Image": "alpine", "Cmd": [ "pm2-runtime", "--watch", "app.js" ], "Name" : prop.hostname}
                      };
                      makeNewContainer(); 
                    } else if (res.statusCode != 200) { //Getting 404 status error when attemping to get log.
                      errorOccured = true;
                      console.log("Container Name: " + prop.hostname);
                      console.log('Status:', res.statusCode);
                    } else {
                      //we need to parse the json response to access
                      console.log("Container stdout = " + data);
                    }
                  });
                }
		          });
            }
          });     
        }
      });
    }
  }
}, 3000);

//Every 60 seconds, check if the time, and scale up or down
setInterval(function(){
  if (systemLeader)
  {
    var currentTime = new Date();
    currentTime.getHours();
    
    if (currentTime >= scaleUpTime && currentTime < scaleDownTime && currentlyScaledUp == false) {
      for (var i = 0; i < 3; i++)
      {
        //populate variable for making a new node.
        create = {
          uri: url + "/v1.40/containers/create",
          method: 'POST',
          //deploy an alpine container that runs my app.js code. Give it a new name.
          data: {"Image": "alpine", "Cmd": [ "pm2-runtime", "--watch", "app.js" ], "Name" : newContainerName + newNodeNameCounter}
        };
        makeNewContainer(); 
        newNodeNameCounter++;
      }
      currentlyScaledUp = true;   
    } else if (currentTime > scaleDownTime && currentlyScaledUp == true) {
      for (var i = 3; i > 0; i--)
      {
        stop = {
          uri: url + "/v1.40/containers/" + newContainerName + newNodeNameCounter + "/stop",
          method: 'POST'
        };
        stopContainer();
        newNodeNameCounter--;
      }
      currentlyScaledUp = false;
    } else {
      console.log("Leader checked container scaling.");
    }
  }
}, 60000);

//Makes a new container from the data structure "create", where the leader populates create and calls the makenewcontainer() (LEADER)
function makeNewContainer()
{
  //send the create request
  request(create, function (error, response, createBody) {
    if (!error) {
      console.log("Created container " + JSON.stringify(createBody));
      //post object for the container start request
      var start = {
        uri: url + "/v1.40/containers/" + createBody.Id + "/start",
        method: 'POST',
        json: {}
      };
      //send the start request, Docker at also handles the restarting of nodes. Need to delete that configuration.
      request(start, function (error, response, startBody) {
        if (!error) {
          console.log("Container start completed");   
          //post object for  wait 
          var wait = {
            uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
            method: 'POST',
            json: {}
          };             
          request(wait, function (error, response, waitBody ) {
            if (!error) {
              console.log("run wait complete, container will have started");           
              request.get({
                url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                }, (err, res, data) => {
                  if (err) {
                    console.log('Error:', err);
                  } else if (res.statusCode != 200) {
                    console.log('Status:', res.statusCode);
                  } else {
                    console.log("Container stdout = " + data);
                  }
              });
            }
          });
        }
      });
    }   
  });
}

function stopContainer(){
  request(stop, function (error, response, stopBody ) {
    if (!error) {
      console.log("Container killed successfully.");           
      //send a simple get request for stdout from the container
    } else {
      console.log("Failed to kill container.");
      console.log("Error: " + response.statusCode);
    }
  });
}
//Publish Alive Messege
setInterval(function() {
  console.log('Node still alive: ' + myhostname);
  //Publish Messege
  amqp.connect('amqp://user:bitnami@RabbitMQ_HAProxy', function(error0, connection) { //need to consider adding hostname to message, so leader knowns container name and can restart node via dockerAPI.
    if (error0) {
      throw error0;
    } else {
      booted = true;
    }
    connection.createChannel(function(error1, channel) {
      if (error1) {
        throw error1;
      }
      var exchange = "alive";
      currentTime = new Date().getTime() / 1000;
      var msg = JSON.stringify({"nodeIdentifier": nodeID, "hostName": myhostname}); //JSON stringify the nodeID, and send it to the Exchange.
      channel.assertExchange(exchange, 'fanout', {
        durable: false
      });
      channel.publish(exchange, '', Buffer.from(msg));
    });
    
    setTimeout(function() {
      connection.close();
    }, 500);
  });
}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

//Schema set up to cater for the data that is needed by the client.
var UserActionSchema = new Schema({
  AccountID: Number,
  UserName: String,
  TitleID: Number,
  UserAction: String,
  DateAndTime: Date,
  InteractionPoint: String,
  InteractionType: String
});

var UserActionModel = mongoose.model('UserInteraction', UserActionSchema, 'userInteraction');

//Send object back for analytics that matches the interaction point / type desired.
app.get('/', (req, res) => {
    UserActionModel.find({},'interactionPoint interactionType dateAndTime', (err, userInteraction) => {
      if(err) return handleError(err);
      res.send(JSON.stringify(userInteraction))
    }) 
  })

//Save a new user interaction.
app.post('/',  (req, res) => {
var newEntry = new userActionModel(req.body);
newEntry.save(function (err) {
if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
});
})

//Publish Messege
amqp.connect('amqp://user:bitnami@RabbitMQ_HAProxy', function(error0, connection) {
if (error0) {
  throw error0;
}
connection.createChannel(function(error1, channel) {
  if (error1) {
    throw error1;
  }
  var exchange = 'logs';
  var msg =  'Hello World!' + nodeID;
  channel.assertExchange(exchange, 'fanout', {
    durable: false
  });
  channel.publish(exchange, '', Buffer.from(msg));
  console.log(" [x] Sent %s", msg);
});

  setTimeout(function() {
    connection.close();
  }, 500);
});

//Subscribe
amqp.connect('amqp://user:bitnami@RabbitMQ_HAProxy', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    var exchange = 'logs';
    channel.assertExchange(exchange, 'fanout', {
      durable: false
    });
    channel.assertQueue('', {
      exclusive: true
    }, function(error2, q) {
      if (error2) {
        throw error2;
      }
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');
      channel.consume(q.queue, function(msg) {
        if(msg.content) {
          console.log(" [x] %s", msg.content.toString());
        }
        }, {
        noAck: true
      });
    });
  });
});
//End of messege queueing service functions.